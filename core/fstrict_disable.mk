# Copyright (C) 2014 The SaberMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_DISABLE_STRICT := \
	libc_bionic \
	libc_dns \
	libc_tzcode \
	libziparchive \
	libtwrpmtp \
	libfusetwrp \
	libguitwrp \
	busybox \
	libuclibcrpc \
	libziparchive-host \
	libpdfiumcore \
	libandroid_runtime \
	libmedia \
	libpdfiumcore \
	libpdfium \
	bluetooth.default \
	logd \
	mdnsd \
	net_net_gyp \
	libstagefright_webm \
	libaudioflinger \
	libmediaplayerservice \
	libstagefright \
	ping \
	ping6 \
	libdiskconfig \
	libjavacore \
	libfdlibm \
	libvariablespeed \
	librtp_jni \
	libwilhelm \
	libdownmix \
	libldnhncr \
	libqcomvisualizer \
	libvisualizer \
	libstlport \
	libutils \
	libandroidfw \
	dnsmasq \
	static_busybox \
	libwebviewchromium \
	libwebviewchromium_loader \
	libwebviewchromium_plat_support \
	content_content_renderer_gyp \
	third_party_WebKit_Source_modules_modules_gyp \
	third_party_WebKit_Source_platform_blink_platform_gyp \
	third_party_WebKit_Source_core_webcore_remaining_gyp \
	third_party_angle_src_translator_lib_gyp \
	third_party_WebKit_Source_core_webcore_generated_gyp \
	libc_gdtoa \
	libc_openbsd \
	libc \
	libc_nomalloc

# bionic
LOCAL_DISABLE_STRICT += \
	libbionic_ssp \
	libc \
	libc_bionic \
	libc_common \
	libc_freebsd \
	libc_malloc_debug_leak \
	libc_malloc_debug_qemu \
	libc_netbsd \
	libc_nomalloc \
	libc_tzcode

# dalvik
LOCAL_DISABLE_STRICT += \
	libdvm \
	libdvm_assert \
	libdvm_interp \
	libdvm_sv

# external/chromium
LOCAL_DISABLE_STRICT += \
	libchromium_net

# external/clang
LOCAL_DISABLE_STRICT += \
	libclangARCMigrate \
	libclangAST \
	libclangAnalysis \
	libclangBasic \
	libclangCodeGen \
	libclangDriver \
	libclangEdit \
	libclangFrontend \
	libclangFrontendTool \
	libclangLex \
	libclangParse \
	libclangRewriteCore \
	libclangRewriteFrontend \
	libclangSema \
	libclangSerialization \
	libclangStaticAnalyzerCheckers \
	libclangStaticAnalyzerCore \
	libclangStaticAnalyzerFrontend

# external/dnsmasq
LOCAL_DISABLE_STRICT += \
	dnsmasq

# external/e2fsprogs
LOCAL_DISABLE_STRICT += \
	e2fsck \
	libext2_blkid \
	libext2_blkid_host

# external/iputils
LOCAL_DISABLE_STRICT += \
	ping \
	ping6

# external/llvm
LOCAL_DISABLE_STRICT += \
	llvm-as \
	llvm-dis \
	llvm-link \
	tblgen

# external/openssl
LOCAL_DISABLE_STRICT += \
	libcrypto \
	libcrypto-host \
	libcrypto_static \
	libssl \
	libssl-host \
	libssl_static

# external/skia
LOCAL_DISABLE_STRICT += \
	libskia \
	libstlport \
	skiatest

# frameworks/compile/slang
LOCAL_DISABLE_STRICT += \
	libslang \
	llvm-rs-cc

# libcore
LOCAL_DISABLE_STRICT += \
	libconscrypt_jni \
	libjavacore \
	libjavacoretests \
	libjavacrypto

ifneq ($(filter $(LOCAL_DISABLE_STRICT), $(LOCAL_MODULE)),)
ifdef LOCAL_CONLYFLAGS
LOCAL_CONLYFLAGS += \
	-fno-strict-aliasing -Wno-strict-aliasing
else
LOCAL_CONLYFLAGS := \
	-fno-strict-aliasing -Wno-strict-aliasing
endif

ifdef LOCAL_CPPFLAGS
LOCAL_CPPFLAGS += \
	-fno-strict-aliasing -Wno-strict-aliasing
else
LOCAL_CPPFLAGS := \
	-fno-strict-aliasing -Wno-strict-aliasing
endif
endif
#####
