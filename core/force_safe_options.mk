##
# force a module to be compiled with safe flags
##

LOCAL_FORCE_SAFE_OPTIONS := \
	bluetooth.default \
	content_content_renderer_gyp \
	libFraunhoferAAC \
	libjavacore \
	libjni_filtershow_filters \
	libmincrypt \
	libminshacrypt \
	libRS \
	libRSDriver\
	libsdcard \
	libspeexresampler \
	libstagefright_amrwbenc \
	libstagefright_m4vh263dec \
	libstagefright_m4vh263enc \
	libstagefright_webm \
	libwebviewchromium \
	libwebviewchromium_loader \
	libwebviewchromium_plat_support \
	libziparchive \
	libziparchive-host \
	net_net_gyp \
	third_party_angle_src_translator_lib_gyp \
	third_party_WebKit_Source_core_webcore_generated_gyp \
	third_party_WebKit_Source_core_webcore_remaining_gyp \
	third_party_WebKit_Source_modules_modules_gyp \
	third_party_WebKit_Source_platform_blink_platform_gyp

# external/bash
LOCAL_FORCE_SAFE_OPTIONS += \
	libsh

ifneq ($(filter $(LOCAL_FORCE_SAFE_OPTIONS), $(LOCAL_MODULE)),)

LOCAL_CONLYFLAGS += \
	-Os -fno-tree-vectorize -fno-modulo-sched -fno-modulo-sched-allow-regmoves -fno-ivopts -fno-strict-aliasing
	
LOCAL_CPPFLAGS += \
	-Os -fno-tree-vectorize -fno-modulo-sched -fno-modulo-sched-allow-regmoves -fno-ivopts -fno-strict-aliasing
	
endif
